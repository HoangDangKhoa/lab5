<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use resources\lang\en\validation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        validation::extends('in_phone', function($attribute, $value, $parameters){
            return substr($value, 0, 3) == "+84";
        });
    }
}
