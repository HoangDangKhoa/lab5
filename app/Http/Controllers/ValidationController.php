<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ValidationController extends Controller
{
    //
    public function showForm() {
        return view('login');
    }

    public function validateForm(Request $rq) {
        print_r($rq->all());
        $this->validate($rq, [
            'username'=>'required|max:8',
            'password'=>'required'
        ]);
    }
}
