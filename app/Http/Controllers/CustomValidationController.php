<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomValidationController extends Controller
{
    public function getCustomValidation() {
        return view('customValidation');
    }

    public function postCustomValidation(Request $rq) {
        $this->validate($rq, [
            'phone'=>'required|in_phone'
        ]);

        return 'successfully';
    }
}
