<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>
<div class="contrainer">
        {{ Form::open(array('route' => 'custom-validation.post','method' => 'POST')) }}

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endoreach
                    </ul>
                </div>
            @endif
        {{ Form::text('phone', old('phone'), ['placeholder' => 'Enter Viet Nam phone number']) }}
        <br>
        {{ Form::submit('save') }}
    </div>
</body>
</html>